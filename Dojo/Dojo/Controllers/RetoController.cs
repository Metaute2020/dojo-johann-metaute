﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dojo.Models;
using Dojo.Models.ViewModels;

namespace Dojo.Controllers
{
    public class RetoController : Controller
    {
        // GET: Reto

        
        public ActionResult Retos(int Id)
        {
            List<ListretosViewModel> listReto;
            
            using (DBDojoEntities db = new DBDojoEntities())
            {
                listReto = (from d in db.Reto_Tbl
                            where d.Id_Dojo == Id
                            select new ListretosViewModel
                            {
                                Id = d.Id,
                                Id_Dojo = d.Id_Dojo,
                                Descripcion = d.Descripcion,
                                Fecha_Apertura = d.Fecha_Apertura,
                                Fecha_Cierre = d.Fecha_Cierre,
                                Valoracion_Reto = d.Valoracion_Reto
                                
                            }).ToList();
            }
            return View(listReto);
        }


        public ActionResult MiembrosReto(int Id /*,int id_dojo*/)
        {
            List<ListMiembroViewModel> listMiembros;

            using (DBDojoEntities db = new DBDojoEntities())
            {
                listMiembros = (from d in db.Miembros_Tbl
                            where d.Id_Reto == Id //&& d.Id_Dojo==id_dojo
                            select new ListMiembroViewModel
                            {
                                Id = d.Id,
                                Id_Dojo = d.Id_Dojo,
                                Nombre = d.Nombre,
                                E_mail = d.E_mail,
                                Id_Reto = d.Id_Reto,
                                Puntuacion_Reto = d.Puntuacion_Reto

                            }).ToList();
            }
            return View(listMiembros);
        }

        public ActionResult NuevoReto()
        {
            return View();
        }


        [HttpPost]
        public ActionResult NuevoReto(RetoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBDojoEntities db = new DBDojoEntities())
                    {
                        var RetoTabla = new Reto_Tbl();
                        RetoTabla.Id_Dojo = model.Id_Dojo;
                        RetoTabla.Descripcion = model.Descripcion;
                        RetoTabla.Fecha_Apertura = model.Fecha_Apertura;
                        RetoTabla.Fecha_Cierre = model.Fecha_Cierre;
                        RetoTabla.Valoracion_Reto = model.Valoracion_Reto;

                        db.Reto_Tbl.Add(RetoTabla);
                        db.SaveChanges();
                    }

                    var ruta = "/Reto/Retos/" + model.Id_Dojo;

                    return Redirect(ruta);
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public ActionResult EditarReto(int Id)
        {
            RetoViewModel model = new RetoViewModel();
            using (DBDojoEntities bd = new DBDojoEntities())
            {
                var oReto = bd.Reto_Tbl.Find(Id);
                model.Id_Dojo = oReto.Id_Dojo;
                model.Descripcion = oReto.Descripcion;
                model.Fecha_Apertura = oReto.Fecha_Apertura;
                model.Fecha_Cierre = oReto.Fecha_Cierre;
                model.Valoracion_Reto = oReto.Valoracion_Reto;
                model.Id = oReto.Id;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditarReto(RetoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBDojoEntities db = new DBDojoEntities())
                    {
                        var retoTabla = db.Reto_Tbl.Find(model.Id);
                        retoTabla.Id_Dojo = model.Id_Dojo;
                        retoTabla.Descripcion = model.Descripcion;
                        retoTabla.Fecha_Apertura = model.Fecha_Apertura;
                        retoTabla.Fecha_Cierre = model.Fecha_Cierre;
                        retoTabla.Valoracion_Reto = model.Valoracion_Reto;
                        retoTabla.Id = model.Id;

                        db.Entry(retoTabla).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    var ruta = "~/Reto/Retos/" + model.Id_Dojo;
                    return Redirect(ruta);
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult EliminarReto(int Id)
        {
            string ruta = null;
            using (DBDojoEntities bd = new DBDojoEntities())
            {
                var oReto = bd.Reto_Tbl.Find(Id);
                bd.Reto_Tbl.Remove(oReto);
                bd.SaveChanges();
                ruta = "~/Reto/Retos/" + oReto.Id_Dojo;
            }

            return Redirect(ruta);
        }
    }
}