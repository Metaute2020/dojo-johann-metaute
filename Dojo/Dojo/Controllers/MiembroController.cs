﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dojo.Models;
using Dojo.Models.ViewModels;

namespace Dojo.Controllers
{
    public class MiembroController : Controller
    {
        // GET: Miembro
        public ActionResult Miembros()
        {
            List<ListMiembroViewModel> listMiembros;
            using (DBDojoEntities db = new DBDojoEntities())
            {
                listMiembros = (from d in db.Miembros_Tbl
                            select new ListMiembroViewModel
                            {
                                Id = d.Id,
                                Id_Dojo = d.Id_Dojo,
                                Nombre = d.Nombre,
                                E_mail = d.E_mail,
                                Id_Reto = d.Id_Reto,
                                Puntuacion_Reto = d.Puntuacion_Reto
                            }).ToList();
            }
            return View(listMiembros);
        }

        public ActionResult AñadirReto()
        {
            List<ListretosViewModel> listRetos;
            using (DBDojoEntities db = new DBDojoEntities())
            {
                listRetos = (from d in db.Reto_Tbl
                             //where d.Id_Dojo == id_dojo
                                select new ListretosViewModel
                                {
                                    Id = d.Id,
                                    Id_Dojo = d.Id_Dojo,
                                    Descripcion = d.Descripcion,
                                    Fecha_Apertura = d.Fecha_Apertura,
                                    Fecha_Cierre = d.Fecha_Cierre,
                                    Valoracion_Reto = d.Valoracion_Reto
                                }).ToList();
            }
            return View(listRetos);
        }

        public ActionResult NuevoMiembro()
        {
            return View();
        }


        [HttpPost]
        public ActionResult NuevoMiembro(MiembroViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBDojoEntities db = new DBDojoEntities())
                    {
                        var miembroTabla = new Miembros_Tbl();
                        miembroTabla.Id_Dojo = model.Id_Dojo;
                        miembroTabla.Nombre = model.Nombre;
                        miembroTabla.E_mail = model.E_mail;
                        miembroTabla.Id_Reto = model.Id_Reto;
                        miembroTabla.Puntuacion_Reto = model.Puntuacion_Reto;
                        
                        db.Miembros_Tbl.Add(miembroTabla);
                        db.SaveChanges();
                    }

                    return Redirect("Miembros/");
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult EditarMiembro(int Id)
        {
            MiembroViewModel model = new MiembroViewModel();
            using (DBDojoEntities bd = new DBDojoEntities())
            {
                var oMiembro = bd.Miembros_Tbl.Find(Id);
                model.Id_Dojo = oMiembro.Id_Dojo;
                model.Nombre = oMiembro.Nombre;
                model.E_mail = oMiembro.E_mail;
                model.Id_Reto = oMiembro.Id_Reto;
                model.Puntuacion_Reto = oMiembro.Puntuacion_Reto;
                model.Id = oMiembro.Id;
            }
            return View(model);
        }

        public ActionResult EditarMiembro(int Id, int id_dojo)
        {
            MiembroViewModel model = new MiembroViewModel();
            using (DBDojoEntities bd = new DBDojoEntities())
            {
                var oMiembro = bd.Miembros_Tbl.Find(Id);
                model.Id_Dojo = oMiembro.Id_Dojo;
                model.Nombre = oMiembro.Nombre;
                model.E_mail = oMiembro.E_mail;
                model.Id_Reto = id_dojo;
                model.Puntuacion_Reto = oMiembro.Puntuacion_Reto;
                model.Id = oMiembro.Id;
            }
            return View(model);
        }



        [HttpPost]
        public ActionResult EditarMiembro(MiembroViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBDojoEntities db = new DBDojoEntities())
                    {
                        var miembroTabla = db.Miembros_Tbl.Find(model.Id);
                        miembroTabla.Id_Dojo = model.Id_Dojo;
                        miembroTabla.Nombre = model.Nombre;
                        miembroTabla.E_mail = model.E_mail;
                        miembroTabla.Id_Reto = model.Id_Reto;
                        miembroTabla.Puntuacion_Reto = model.Puntuacion_Reto;
                        miembroTabla.Id = model.Id;

                        db.Entry(miembroTabla).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    var ruta = "~/Miembro/Miembros/" + model.Id_Dojo;
                    return Redirect(ruta);
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult EliminarMiembro(int Id)
        {
            string ruta = null;
            using (DBDojoEntities bd = new DBDojoEntities())
            {
                var oMiembro = bd.Miembros_Tbl.Find(Id);
                bd.Miembros_Tbl.Remove(oMiembro);
                bd.SaveChanges();
                ruta = "~/Miembro/Miembros/";
            }

            return Redirect(ruta);
        }
    }
}