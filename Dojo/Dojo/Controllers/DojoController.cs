﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dojo.Models;
using Dojo.Models.ViewModels;

namespace Dojo.Controllers
{
    public class DojoController : Controller
    {
        // GET: Dojo
        public ActionResult Dojos()
        {
            List<ListdojoViewModel> listDojo;
            using(DBDojoEntities db = new DBDojoEntities())
            {
                listDojo = (from d in db.Dojo_Tbl
                            select new ListdojoViewModel
                            {
                                Id = d.Id,
                                Nombre = d.Nombre,
                                Descripcion = d.Descripcion
                            }).ToList();
            }
            return View(listDojo);
        }

        public ActionResult NuevoDojo()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult NuevoDojo(DojoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBDojoEntities db = new DBDojoEntities())
                    {
                        var dojoTabla = new Dojo_Tbl();
                        dojoTabla.Nombre = model.Nombre;
                        dojoTabla.Descripcion = model.Descripcion;

                        db.Dojo_Tbl.Add(dojoTabla);
                        db.SaveChanges();
                    }

                    return Redirect("Dojos/");
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult EditarDojo(int Id)
        {
            DojoViewModel model = new DojoViewModel();
            using(DBDojoEntities bd = new DBDojoEntities())
            {
                var oDojo = bd.Dojo_Tbl.Find(Id);
                model.Nombre = oDojo.Nombre;
                model.Descripcion = oDojo.Descripcion;
                model.Id = oDojo.Id;
            }
            
            return View(model);
        }


        [HttpPost]
        public ActionResult EditarDojo(DojoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DBDojoEntities db = new DBDojoEntities())
                    {
                        var dojoTabla = db.Dojo_Tbl.Find(model.Id);
                        dojoTabla.Nombre = model.Nombre;
                        dojoTabla.Descripcion = model.Descripcion;
                        dojoTabla.Id = model.Id;

                        db.Entry(dojoTabla).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }

                    return Redirect("~/Dojo/Dojos");
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public ActionResult EliminarDojo(int Id)
        {
            using (DBDojoEntities bd = new DBDojoEntities())
            {
                var oDojo = bd.Dojo_Tbl.Find(Id);
                bd.Dojo_Tbl.Remove(oDojo);
                bd.SaveChanges();
                
            }

            return Redirect("~/Dojo/Dojos");
        }
    }
}