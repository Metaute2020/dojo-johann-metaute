//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dojo.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Reto_Tbl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Reto_Tbl()
        {
            this.Miembros_Tbl = new HashSet<Miembros_Tbl>();
        }
    
        public int Id { get; set; }
        public int Id_Dojo { get; set; }
        public string Descripcion { get; set; }
        public System.DateTime Fecha_Apertura { get; set; }
        public System.DateTime Fecha_Cierre { get; set; }
        public Nullable<int> Valoracion_Reto { get; set; }
    
        public virtual Dojo_Tbl Dojo_Tbl { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Miembros_Tbl> Miembros_Tbl { get; set; }
    }
}
