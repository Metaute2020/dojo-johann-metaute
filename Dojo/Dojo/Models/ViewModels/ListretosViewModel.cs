﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dojo.Models.ViewModels
{
    public class ListretosViewModel
    {
        public int Id { get; set; }
        public int Id_Dojo { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha_Apertura { get; set; }
        public DateTime Fecha_Cierre { get; set; }
        public int ?Valoracion_Reto { get; set; }
    }
}