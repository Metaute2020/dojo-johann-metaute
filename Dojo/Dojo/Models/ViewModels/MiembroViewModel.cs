﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dojo.Models.ViewModels
{
    public class MiembroViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "#Dojo*")]
        public int Id_Dojo { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Nombre*")]
        public string Nombre { get; set; }
        [Required]
        [StringLength(50)]
        [EmailAddress]
        [Display(Name = "E-mail*")]
        public string E_mail { get; set; }
        [Display(Name = "Reto")]
        public int? Id_Reto { get; set; }
        [Display(Name = "Puntuación en el reto")]
        public int? Puntuacion_Reto { get; set; }
    }
}