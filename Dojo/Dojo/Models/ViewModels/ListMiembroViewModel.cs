﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dojo.Models.ViewModels
{
    public class ListMiembroViewModel
    {
        public int Id { get; set; }
        public int Id_Dojo { get; set; }
        public string Nombre { get; set; }
        public string E_mail { get; set; }
        public int? Id_Reto { get; set; }
        public int? Puntuacion_Reto { get; set; }
    }
}