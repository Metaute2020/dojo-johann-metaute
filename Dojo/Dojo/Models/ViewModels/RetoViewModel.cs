﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dojo.Models.ViewModels
{
    public class RetoViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "#Dojo")]
        public int Id_Dojo { get; set; }
        [Required]
        [StringLength(500)]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha apertura")]
        public DateTime Fecha_Apertura { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha cierre")]
        public DateTime Fecha_Cierre { get; set; }
        [Required]
        [Display(Name = "Valoración del reto")]
        public int? Valoracion_Reto { get; set; }
    }
}