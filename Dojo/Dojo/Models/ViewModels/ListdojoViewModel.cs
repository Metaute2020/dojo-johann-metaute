﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dojo.Models.ViewModels
{
    public class ListdojoViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}