USE [master]
GO
/****** Object:  Database [DBDojo]    Script Date: 12/01/2021 2:21:47 a. m. ******/
CREATE DATABASE [DBDojo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBDojo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DBDojo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DBDojo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DBDojo_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [DBDojo] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBDojo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBDojo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBDojo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBDojo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBDojo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBDojo] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBDojo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBDojo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBDojo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBDojo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBDojo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBDojo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBDojo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBDojo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBDojo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBDojo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBDojo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBDojo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBDojo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBDojo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBDojo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBDojo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBDojo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBDojo] SET RECOVERY FULL 
GO
ALTER DATABASE [DBDojo] SET  MULTI_USER 
GO
ALTER DATABASE [DBDojo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBDojo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBDojo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBDojo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DBDojo] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DBDojo', N'ON'
GO
ALTER DATABASE [DBDojo] SET QUERY_STORE = OFF
GO
USE [DBDojo]
GO
/****** Object:  Table [dbo].[Dojo_Tbl]    Script Date: 12/01/2021 2:21:47 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dojo_Tbl](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Dojo_Tbl] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Miembros_Tbl]    Script Date: 12/01/2021 2:21:47 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Miembros_Tbl](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Dojo] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[E_mail] [varchar](50) NOT NULL,
	[Id_Reto] [int] NULL,
	[Puntuacion_Reto] [int] NULL,
 CONSTRAINT [PK_Miembros_Tble] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reto_Tbl]    Script Date: 12/01/2021 2:21:47 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reto_Tbl](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Dojo] [int] NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[Fecha_Apertura] [datetime] NOT NULL,
	[Fecha_Cierre] [datetime] NOT NULL,
	[Valoracion_Reto] [int] NULL,
 CONSTRAINT [PK_Reto_Tbl] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sensei_Tbl]    Script Date: 12/01/2021 2:21:47 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sensei_Tbl](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Dojo] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Sensei_Tbl] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Dojo_Tbl] ON 
GO
INSERT [dbo].[Dojo_Tbl] ([Id], [Nombre], [Descripcion]) VALUES (1, N'Liga de desarrollo de Software', N'En este dojo vamos a realizar las practicas para reconocer los conocimientos prácticos de los aprendices para la liga de desarrollo de Software.')
GO
INSERT [dbo].[Dojo_Tbl] ([Id], [Nombre], [Descripcion]) VALUES (2, N'Liga de Calidad y Testing', N'En este dojo vamos a realizar las practicas para reconocer los conocimientos prácticos de los aprendices para la liga de calidad y testing')
GO
INSERT [dbo].[Dojo_Tbl] ([Id], [Nombre], [Descripcion]) VALUES (10, N'Dojo de Programación web', N'Este dojo es para programadores web')
GO
INSERT [dbo].[Dojo_Tbl] ([Id], [Nombre], [Descripcion]) VALUES (11, N'Dojo Prueba', N'Prueba')
GO
SET IDENTITY_INSERT [dbo].[Dojo_Tbl] OFF
GO
SET IDENTITY_INSERT [dbo].[Miembros_Tbl] ON 
GO
INSERT [dbo].[Miembros_Tbl] ([Id], [Id_Dojo], [Nombre], [E_mail], [Id_Reto], [Puntuacion_Reto]) VALUES (1, 2, N'Johann Metaute', N'jmetaute1@gmail.com', 5, NULL)
GO
INSERT [dbo].[Miembros_Tbl] ([Id], [Id_Dojo], [Nombre], [E_mail], [Id_Reto], [Puntuacion_Reto]) VALUES (2, 2, N'Stiven Metaute', N'smetaut@gmail.com', 5, 5)
GO
INSERT [dbo].[Miembros_Tbl] ([Id], [Id_Dojo], [Nombre], [E_mail], [Id_Reto], [Puntuacion_Reto]) VALUES (4, 1, N'Manuela Fernández', N'mfernandez@gmail.com', 1, 100)
GO
INSERT [dbo].[Miembros_Tbl] ([Id], [Id_Dojo], [Nombre], [E_mail], [Id_Reto], [Puntuacion_Reto]) VALUES (5, 10, N'Carlos Santana', N'csantana@gmail.com', 17, 10)
GO
INSERT [dbo].[Miembros_Tbl] ([Id], [Id_Dojo], [Nombre], [E_mail], [Id_Reto], [Puntuacion_Reto]) VALUES (6, 1, N'Jairo Hernandez', N'jhernandez@gmail.com', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Miembros_Tbl] OFF
GO
SET IDENTITY_INSERT [dbo].[Reto_Tbl] ON 
GO
INSERT [dbo].[Reto_Tbl] ([Id], [Id_Dojo], [Descripcion], [Fecha_Apertura], [Fecha_Cierre], [Valoracion_Reto]) VALUES (1, 1, N'Crear dojos, que los dojos puedan tener retos y miembros.', CAST(N'2021-01-06T00:00:00.000' AS DateTime), CAST(N'2021-01-19T00:00:00.000' AS DateTime), 100)
GO
INSERT [dbo].[Reto_Tbl] ([Id], [Id_Dojo], [Descripcion], [Fecha_Apertura], [Fecha_Cierre], [Valoracion_Reto]) VALUES (5, 2, N'Reto hacer el almuerzo', CAST(N'2021-01-06T00:00:00.000' AS DateTime), CAST(N'2021-01-13T00:00:00.000' AS DateTime), 1000)
GO
INSERT [dbo].[Reto_Tbl] ([Id], [Id_Dojo], [Descripcion], [Fecha_Apertura], [Fecha_Cierre], [Valoracion_Reto]) VALUES (6, 2, N'Prueba', CAST(N'2021-01-06T00:00:00.000' AS DateTime), CAST(N'2021-01-06T00:00:00.000' AS DateTime), 100)
GO
INSERT [dbo].[Reto_Tbl] ([Id], [Id_Dojo], [Descripcion], [Fecha_Apertura], [Fecha_Cierre], [Valoracion_Reto]) VALUES (14, 2, N'Prueba', CAST(N'2020-12-10T00:00:00.000' AS DateTime), CAST(N'2020-12-09T00:00:00.000' AS DateTime), 10)
GO
INSERT [dbo].[Reto_Tbl] ([Id], [Id_Dojo], [Descripcion], [Fecha_Apertura], [Fecha_Cierre], [Valoracion_Reto]) VALUES (17, 10, N'Reto de cocinar', CAST(N'2021-01-05T00:00:00.000' AS DateTime), CAST(N'2021-01-12T00:00:00.000' AS DateTime), 50)
GO
INSERT [dbo].[Reto_Tbl] ([Id], [Id_Dojo], [Descripcion], [Fecha_Apertura], [Fecha_Cierre], [Valoracion_Reto]) VALUES (18, 1, N'Reto de hacer memes', CAST(N'2021-01-05T00:00:00.000' AS DateTime), CAST(N'2021-01-13T00:00:00.000' AS DateTime), 10)
GO
SET IDENTITY_INSERT [dbo].[Reto_Tbl] OFF
GO
ALTER TABLE [dbo].[Miembros_Tbl]  WITH CHECK ADD  CONSTRAINT [FK_Miembros_Tble_Dojo_Tbl] FOREIGN KEY([Id_Dojo])
REFERENCES [dbo].[Dojo_Tbl] ([Id])
GO
ALTER TABLE [dbo].[Miembros_Tbl] CHECK CONSTRAINT [FK_Miembros_Tble_Dojo_Tbl]
GO
ALTER TABLE [dbo].[Miembros_Tbl]  WITH CHECK ADD  CONSTRAINT [FK_Miembros_Tble_Reto_Tbl] FOREIGN KEY([Id_Reto])
REFERENCES [dbo].[Reto_Tbl] ([Id])
GO
ALTER TABLE [dbo].[Miembros_Tbl] CHECK CONSTRAINT [FK_Miembros_Tble_Reto_Tbl]
GO
ALTER TABLE [dbo].[Reto_Tbl]  WITH CHECK ADD  CONSTRAINT [FK_Reto_Tbl_Dojo_Tbl] FOREIGN KEY([Id_Dojo])
REFERENCES [dbo].[Dojo_Tbl] ([Id])
GO
ALTER TABLE [dbo].[Reto_Tbl] CHECK CONSTRAINT [FK_Reto_Tbl_Dojo_Tbl]
GO
ALTER TABLE [dbo].[Sensei_Tbl]  WITH CHECK ADD  CONSTRAINT [FK_Sensei_Tbl_Dojo_Tbl] FOREIGN KEY([Id_Dojo])
REFERENCES [dbo].[Dojo_Tbl] ([Id])
GO
ALTER TABLE [dbo].[Sensei_Tbl] CHECK CONSTRAINT [FK_Sensei_Tbl_Dojo_Tbl]
GO
USE [master]
GO
ALTER DATABASE [DBDojo] SET  READ_WRITE 
GO
